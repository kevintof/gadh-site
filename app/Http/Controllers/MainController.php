<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class MainController extends Controller
{
    private $months;

    public function __construct(){
        $this->months =["Jan.", "Fév.","Mars","Avr.","Mai","Juin","Juil","Aout","Sept.","Oct.","Nov.","Déc."];
    }
    
    public function index()
    {
        $postsDesc = Post::where('type_id',1)->get();

        $enseignements = Post::where('type_id',2)
                                ->orderBy('date','desc')
                                ->limit(3)
                                ->get();
        $events = Post::where('type_id',3)->get();
        return response()->view('frontend.index',[
            "postDescs" => $postsDesc,
            "events" => $events,
            "enseignements" => $enseignements,
            "months" => $this->months
        ]);
    }

    public function contact()
    {
        return response()->view('frontend.pages.contact');
    }
    
    public function events()
    {
        return response()->view('frontend.pages.events');
    }

    public function sermons(){
        return response()->view('frontend.pages.enseignements');
    }

    public function sermon_details(){
       return response()->view('frontend.pages.detail_enseignement');        
    }

    public function sermon_show(Request $request,$id ){
        $sermon = Post::find($id);
        $posts = Post::where('type_id',2)->orderBy('id','DESC')->limit(3)->get();
        return response()->view('frontend.pages.detail_enseignement',[
            "sermon" => $sermon,
            "latestPosts" => $posts
        ]);
    }

    public function event_show(Request $request,$id)
    {
        $event = Post::find($id);
        $latestEvents = Post::where('type_id',3)->orderBy('id','DESC')->limit(4)->get();
        return response()->view('frontend.pages.detail_evenement',[
            "event" => $event,
            "latestEvents" => $latestEvents,
            "months" => $this->months
        ]);
    }

    public function churches(){
        return response()->view('frontend.pages.churches');
    }

    public function post_details(Request $request,$id)
    {
        $post = Post::find($id);
    }
}
