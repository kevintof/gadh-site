<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PaymentController extends Controller
{
    //

    public function pagePayment(Request $request)
    {
        return response()->view('frontend.pages.payments.index');
    }
}
