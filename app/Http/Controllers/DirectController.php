<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DirectController extends Controller
{
    public function direct()
    {
        
        return response()->view('frontend.pages.direct.direct',[
            "isOpened" => "",
            "link" => "", 
        ]);
    }
    
}
