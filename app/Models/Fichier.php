<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Fichier extends Model
{
    
    protected $table = 'gadh_fichiers';
    protected $fillable = [
        'nom',
        'telegram_file',
        'is_document',
        'is_audio',
        'post_id'
    ];

    public $timestamps = false;
    
    

}




?>