<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    
    protected $table = 'messages';
    protected $fillable = [
        'nom_complet',
        'email',
        'numero_telephone',
        'message'
    ];

    public $timestamps = false;
    
    

}
