<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Cotisation extends Model
{
    
    protected $table = 'gadh_cotisations';
    protected $fillable = [
        'nom',
        'prenom',
        'cotisation_type_id',
        'amount',
        'groupe_id',
        'transaction_id'
    ];

    public $timestamps = false;
    
    

}




?>