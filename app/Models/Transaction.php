<?php 
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    
    protected $table = 'gadh_fichiers';
    protected $fillable = [
        'agregator_id',
        'number',
        'email',
        'amount',
        'state_id',
        'commentaires',
        'transaction_identifier',
        

    ];

    public $timestamps = false;
    
    

}




?>