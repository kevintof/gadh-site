<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Post extends Model{
    protected $table = 'gadh_posts';

    protected $fillable = [
        'title',
        'body',
        'orateur',
        'date',
        'heure_debut',
        'heure_fin',
        'type_id',
        'cover_image',
        'brief' 
    ];

    public function type()
    {
        return $this->belongsTo('App\Models\PostType');
    }

    public function fichiers()
    {
        return $this->hasMany('App\Models\Fichier');
    }




     
}



?> 