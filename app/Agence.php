<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Agence extends Model
{
    //
    public function Guichets()
    {
        return $this->hasMany('App\Guichet');
    }
}
