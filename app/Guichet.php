<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guichet extends Model
{
    //

    protected $fillable = [
        'code'
    ];
    
    public function agence()
    {
        return $this->belongsTo('App\Agence');
    }

}
