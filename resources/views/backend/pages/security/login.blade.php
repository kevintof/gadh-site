<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>{% block title %}Welcome!{% endblock %}</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
		<meta charset="utf-8" />
		<meta property="og:locale" content="en_US" />
		<meta property="og:type" content="article" />
        <meta name="keywords" content="licence sportive, CNO, comité nationale olympique CNO">
        <link rel="shortcut icon" href="asset('themes/media/logos/favicon.ico')" />
        <!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->
        <link href="{{asset('js/backend/js/plugins/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />
		<!--end::Page Vendor Stylesheets-->
		<!--begin::Global Stylesheets Bundle(used by all pages)-->
		<link href="{{asset('js/backend/js/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
		<link href="{{asset('css/backend/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
        <!--begin::Global Stylesheets Bundle(used by all pages)-->
		
		<!--end::Global Stylesheets Bundle-->
        

    </head>
    <body id="kt_body" class="bg-body">
        <div class="d-flex flex-column flex-root">
            <div class="d-flex flex-column flex-lg-row flex-column-fluid">
               <!--begin::Aside-->
			   
				<div class="d-flex flex-column flex-lg-row-auto w-xl-600px w-xxl-800px positon-xl-relative" style="background-color: #FFF">
					<!--begin::Wrapper-->
					<div class="d-flex flex-column position-xl-fixed top-0 bottom-0 w-xl-600px w-xxl-800px">
						<!--begin::Content-->
						<div class="d-flex flex-row-fluid flex-column text-center">
							<!--begin::Logo-->
							{# <a href="#" class="py-9 mb-5">
								<img alt="Logo" src="{{asset('img/cno.png')}}" class="h-150px" />
							</a>
							<!--end::Logo-->
							<!--begin::Title-->
							<div class="container">
								<div class="row row-centered">
									<div class="col-lg-12 col-xs-12 text-center">
										<h3 class="fw-bolder fs-2qx pb-5 pb-md-10" style="color: #986923;">Bienvenue sur la plateforme de gestion des licences sportive du CNO Togo.</h3>	
									</div>
								</div>
								
							</div> #}

							<div>
							</div>

							<div class="stack-top stack-length">
								<img src="{{asset('img/login_files_desktop/cno-logo.png')}}" class="img-fluid img-width"/>
							</div>
							<div id="carouselExampleControlsNoTouching" class="carousel slide h-100" data-bs-touch="false" data-bs-interval="false">
								<div class="carousel-inner" >
									<div class="carousel-item active">
									<img src="{{asset('img/gadh.png')}}" height="100%" class="d-block w-100 img-responsive w-100" alt="...">
									</div>
									<div class="carousel-item">
										<img src="{{asset('img/gadh.png')}}" height="100%" class="d-block img-responsive w-100" alt="...">
									</div>
									<div class="carousel-item">
									  <img src="{{asset('img/gadh.png')}}"  height="100%" class="d-block  img-responsive w-100" alt="...">
									</div>
									<div class="carousel-item">
									  <img src="{{asset('img/gadh.png')}}" style="max-height:100%;" height="100%" class="d-block w-100 img-responsive" alt="...">
									</div>
									<div class="carousel-item">
									  <img src="{{asset('img/gadh.png')}}" style="max-height:100%;" height="100%" class="d-block w-100 img-responsive" alt="...">
									</div>
								</div>
								<button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControlsNoTouching" data-bs-slide="prev">
									<span class="carousel-control-prev-icon" aria-hidden="true"></span>
									<span class="visually-hidden">Previous</span>
								</button>
								<button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControlsNoTouching" data-bs-slide="next">
									<span class="carousel-control-next-icon" aria-hidden="true"></span>
									<span class="visually-hidden">Next</span>
								</button>
							</div>
							<div id="carouselMobile" class="carousel slide h-100" data-bs-touch="false" data-bs-interval="false">
								<div class="carousel-inner" >
									<div class="carousel-item active">
										<img src="{{asset('img/gadh.png')}}" height="100%" class="d-block w-100 img-responsive w-100" alt="...">
									</div>
									{# <div class="carousel-item">
										<img src="{{asset('img/gadh.png')}}" height="100%" class="d-block img-responsive w-100" alt="...">
									</div> #}
									<div class="carousel-item">
									  <img src="{{asset('img/gadh.png')}}"  height="100%" class="d-block  img-responsive w-100" alt="...">
									</div>
									<div class="carousel-item">
									  <img src="{{asset('img/gadh.png')}}" style="max-height:100%;" height="100%" class="d-block w-100 img-responsive" alt="...">
									</div>
									<div class="carousel-item">
									  <img src="{{asset('img/gadh.png')}}" style="max-height:100%;" height="100%" class="d-block w-100 img-responsive" alt="...">
									</div>
								</div>
								<button class="carousel-control-prev" type="button" data-bs-target="#carouselMobile" data-bs-slide="prev">
									<span class="carousel-control-prev-icon" aria-hidden="true"></span>
									<span class="visually-hidden">Previous</span>
								</button>
								<button class="carousel-control-next" type="button" data-bs-target="#carouselMobile" data-bs-slide="next">
									<span class="carousel-control-next-icon" aria-hidden="true"></span>
									<span class="visually-hidden">Next</span>
								</button>
							</div>
							


							
							<!--end::Title-->
							<!--begin::Description-->
							<p class="fw-bold fs-2" style="color: #986923;">
							{# <br />with great build tools</p> #}
							<!--end::Description-->
						</div>
						<!--end::Content-->
						<!--begin::Illustration-->
						{# <div class="d-flex flex-row-auto bgi-no-repeat bgi-position-x-center bgi-size-contain bgi-position-y-bottom min-h-100px min-h-lg-350px" style="background-image: url({{asset('themes/media/illustrations/dozzy-1/13.png')}})"></div> #}
						<!--end::Illustration-->
					</div>
					<!--end::Wrapper-->
				</div>
				<!--end::Aside-->
                <!--begin::Body-->
				<div class="d-flex flex-column flex-lg-row-fluid py-10">
					<!--begin::Content-->
					<div class="d-flex flex-center flex-column flex-column-fluid">
						<!--begin::Wrapper-->
                        <div class="wrapper">
                            <div class="block-center mt-4 wd-xl">
                               <!-- START card-->
                               <div class="card card-flat">
                                  <div class="card-header text-center bg-dark"><a href="#">SEMOA TOGO</a></div>
                                  <div class="card-body">
                                     <p class="text-center py-2">Connectez vous pour continuer</p>
                      
                                     
                                      
                      
                                     <form class="mb-3" method="POST" novalidate>
                                        <div class="form-group">
                                           <div class="input-group with-focus">
                                              <input class="form-control border-right-0" id="exampleInputEmail1" name="email" 
                                                      type="email" placeholder="Enter email" 
                                                      value="" 
                                                      autocomplete="off" required>
                                              <div class="input-group-append"><span class="input-group-text text-muted bg-transparent border-left-0"><em class="fa fa-envelope"></em></span></div>
                                           </div>
                                        </div>
                                        <div class="form-group">
                                           <div class="input-group with-focus"><input class="form-control border-right-0" id="exampleInputPassword1" name="password" type="password" placeholder="Password" required>
                                              <div class="input-group-append"><span class="input-group-text text-muted bg-transparent border-left-0"><em class="fa fa-lock"></em></span></div>
                                           </div>
                                        </div>
                                        <input type="hidden" name="csrf_token" value="{{ csrf_token('login_form') }}">
                                        <div class="clearfix">
                                          <div class="checkbox c-checkbox float-left mt-0"><label><input type="checkbox" value="" name="_remember_me"><span class="fa fa-check"></span> Garder ma session active</label></div>
                                          <div class="float-right"><a class="text-muted" href="">Mot de passe oublié?</a></div>
                                        </div><button class="btn btn-block btn-primary mt-3" type="submit">Connexion</button>
                                     </form>
                                  </div>
                               </div><!-- END card-->
                               
                            </div>
                         </div>
                            <!--end::Wrapper-->
                        </div>
					<!--end::Content-->
					<!--begin::Footer-->
					<div class="d-flex flex-center flex-wrap fs-6 p-5 pb-0">
						<!--begin::Links-->
						<div class="d-flex flex-center fw-bold fs-6">
							<a href="#" class="text-muted text-hover-primary px-2" target="_blank">A propos</a>
							<a href="#" class="text-muted text-hover-primary px-2" target="_blank">Support</a>
							
						</div>
						<!--end::Links-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Body--> 
                
                

            </div>
        
        
        </div>

        

        
        <!-- =============== APP SCRIPTS ===============-->
        

        <script src="{{ asset('bundles/fosjsrouting/js/router.min.js') }}"></script>
       
        
        

        <!--begin::Javascript-->
		<!--begin::Global Javascript Bundle(used by all pages)-->
		<script src="{{asset('js/backend/plugins/global/plugins.bundle.js')}}"></script>
		<script src="{{asset('js/backend/js/scripts.bundle.js')}}"></script>
		<!--end::Global Javascript Bundle-->
		<!--begin::Page Vendors Javascript(used by this page)-->
        <script src="{{asset('js/backend/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}"></script>

		<!--end::Page Vendors Javascript-->
        
		<!--begin::Page Custom Javascript(used by this page)-->
		
		<script src="{{asset('themes/js/custom/authentication/sign-in/general.js')}}"></script>
        <!--end::Page Custom Javascript-->
		<!--end::Javascript-->
        
    </body>
</html>
