@extends('frontend.layouts.app')
@section('headTitle')
     Eglises
 @endsection
 
 @section('content')
 <div class="about-us-area about-page section-padding-100">
    <div class="container">
        <div class="row align-items-center justify-content-between">
            <div class="col-12 col-lg-5">
                <div class="about-content">
                    <h2>Chapelle la vérité libère</h2>
                    <p>It’s very important to believe that you’re the one. We aim to take a different approach in reaching out and helping people along the journey. We stress cultural relevancy and utilize all medias and means to communicate the church message. It’s very important to believe that you’re the one.</p>
                    <div class="opening-hours-location mt-30 d-flex align-items-center">
                        <!-- Opening Hours -->
                        <div class="opening-hours">
                            <h6><i class="fa fa-clock-o"></i> Heure d'ouverture</h6>
                            <p>Mon - Fri at 08:00 - 21:00 <br>Sunday at 09:00 - 18:00</p>
                        </div>
                        <!-- Location -->
                        <div class="location">
                            <h6><i class="fa fa-map-marker"></i> Lieu</h6>
                            <p>Agbalépédogan, derrière haac au bord des rails</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="about-thumbnail">
                    <img src="img/bg-img/26.jpg" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ##### Latest Sermons Area Start ##### -->
<!-- ##### Latest Sermons Area End ##### -->

<div class="team-members-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <!-- Section Heading -->
                <div class="col-12">
                    <div class="section-heading">
                        <h2>Equipe dirigeante</h2>
                        <p>A brief overview of what you can expect at our worship experiences.</p>
                    </div>
                </div>
            </div>
            <div data-aos="fade-down-left" data-aos-duration="3000">
                <div class="row">
                    <!-- Team Members Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="single-team-members text-center mb-100">
                            <div class="team-thumb" style="background-image: url(img/bg-img/33.jpg);">
                                <div class="team-social-info">
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <h6>KASSEGNE Joseph</h6>
                            <span>Pasteur fondateur</span>
                        </div>
                    </div>
    
                    <!-- Team Members Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="single-team-members text-center mb-100">
                            <div class="team-thumb" style="background-image: url(img/bg-img/34.jpg);">
                                <div class="team-social-info">
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <h6>KASSEGNE Dorcas</h6>
                            <span>Pasteur</span>
                        </div>
                    </div>
    
                    <!-- Team Members Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="single-team-members text-center mb-100">
                            <div class="team-thumb" style="background-image: url(img/bg-img/35.jpg);">
                                <div class="team-social-info">
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <h6>WELBECK Isidore</h6>
                            <span>pasteur</span>
                        </div>
                    </div>
    
                    <!-- Team Members Area -->
                    <div class="col-12 col-sm-6 col-lg-3">
                        <div class="single-team-members text-center mb-100">
                            <div class="team-thumb" style="background-image: url(img/bg-img/36.jpg);">
                                <div class="team-social-info">
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <h6>LAWSON Esther</h6>
                            <span>Présidente des femmes</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 @endsection