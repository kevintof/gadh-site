@extends('frontend.layouts.app')
@section("subbreadcrumb")
 GADH TV
@endsection
@section('headTitle')
   GADH TV
@endsection
@section('content')
    <!-- ##### Sermons Area Start ##### -->
    <div class="sermons-details-area section-padding-100">
        <div class="container">
            <div class="row justify-content-between">
                <!-- Blog Posts Area -->
                <div class="col-12 col-lg-12">
                    <div class="sermons-details-area">

                        <!-- Sermons Details Area -->
                        <div class="">
                            <div class="post-content">
                                {{-- <h2 class="post-title mb-20"></h2> --}}
                                
                                <!-- Catagory & Share -->
                                <div class="catagory-share-meta d-flex flex-wrap justify-content-between align-items-center">
                                    
                                </div>
                                    
                                    <iframe src="http://3.231.127.81:8080/gadhtv.mp4" id="gadh-screen" class="gadh-screen" title="W3Schools Free Online Web Tutorials">

                                    </iframe>
                                
                                    

                                    
                                    <div class="disconnected">
                                        <div class="row">
                                            <div class="col-lg-12 ">
                                                <img class="unplugged" src="{{asset('img/unplugged.svg')}}"/>
                                                <div class="text-center broadcast-message animate__animated animate__backInRight animate__delay-5s mt-2">
                                                    Aucune diffusion n'est cours. Veuillez vous connecter ultérieurement
                                                </div>
                                            </div>
                                            
                                        </div>

                                    </div>
                                {{-- <blockquote>
                                    <div class="blockquote-text">
                                        <h6>“There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.” </h6>
                                        <h6>Ollie Schneider - <span>Parson</span></h6>
                                    </div>
                                </blockquote> --}}
                                
                            </div>
                        </div>

                        <!-- Comment Area Start -->
                        <!--<div class="comment_area clearfix">
                            <ol>-->
                                <!-- Single Comment Area -->
                            <!--    <li class="single_comment_area">
                                    <div class="comment-wrapper d-flex">-->
                                        <!-- Comment Meta -->
                                        <!--<div class="comment-author">
                                            <img src="img/bg-img/28.jpg" alt="">
                                        </div>-->
                                        <!-- Comment Content -->
                                       <!-- <div class="comment-content">
                                            <span class="comment-date">March 15, 2018</span>
                                            <h5>Lena Headey</h5>
                                            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.</p>
                                            <a href="#">Like</a>
                                            <a href="#">Reply</a>
                                        </div>
                                    </div>
                                </li>
                            </ol>
                        </div>-->

                         <!-- Leave A Comment --> 
                        <!-- <div class="leave-comment-area mt-50 clearfix">
                            <div class="comment-form">
                                <h4 class="headline">Leave A Comment</h4>
                                
                                
                                <div class="contact-form-area">
                                    <form action="#" method="post">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="contact-name" placeholder="Name">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="contact-email" placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="contact-number" placeholder="Website">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <textarea class="form-control" name="message" id="message" cols="30" rows="10" placeholder="Message"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <button type="submit" class="btn crose-btn mt-15">Post Comment</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div> -->
                    </div>
                </div>

                <!-- Blog Sidebar Area -->

                
            </div>
        </div>
    </div>
    <!-- ##### Sermons Area End ##### -->
@endsection
@section('js')
   <script src="{{asset('js/checkBroadcasting.js')}}"></script>
@endsection