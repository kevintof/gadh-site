@extends('frontend.layouts.app')
@section("subbreadcrumb")
 {{$sermon->title}}
@endsection
@section('headTitle')
   Details enseignements
@endsection
@section('content')
    <!-- ##### Sermons Area Start ##### -->
    <div class="sermons-details-area section-padding-100">
        <div class="container">
            <div class="row justify-content-between">
                <!-- Blog Posts Area -->
                <div class="col-12 col-lg-8">
                    <div class="sermons-details-area">

                        <!-- Sermons Details Area -->
                        <div class="single-post-details-area">
                            <div class="post-content">
                                <h2 class="post-title mb-20">{{ $sermon->title }}</h2>
                                
                                <!-- Catagory & Share -->
                                <div class="catagory-share-meta d-flex flex-wrap justify-content-between align-items-center">
                                    
                                </div>
                                
                                        <p class="lh-lg" style="line-height:3!important">{{ $sermon->body }}</p>
                                {{-- <blockquote>
                                    <div class="blockquote-text">
                                        <h6>“There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable.” </h6>
                                        <h6>Ollie Schneider - <span>Parson</span></h6>
                                    </div>
                                </blockquote> --}}
                                
                            </div>
                        </div>

                        <!-- Comment Area Start -->
                        <!--<div class="comment_area clearfix">
                            <ol>-->
                                <!-- Single Comment Area -->
                            <!--    <li class="single_comment_area">
                                    <div class="comment-wrapper d-flex">-->
                                        <!-- Comment Meta -->
                                        <!--<div class="comment-author">
                                            <img src="img/bg-img/28.jpg" alt="">
                                        </div>-->
                                        <!-- Comment Content -->
                                       <!-- <div class="comment-content">
                                            <span class="comment-date">March 15, 2018</span>
                                            <h5>Lena Headey</h5>
                                            <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore.</p>
                                            <a href="#">Like</a>
                                            <a href="#">Reply</a>
                                        </div>
                                    </div>
                                </li>
                            </ol>
                        </div>-->

                         <!-- Leave A Comment --> 
                        <!-- <div class="leave-comment-area mt-50 clearfix">
                            <div class="comment-form">
                                <h4 class="headline">Leave A Comment</h4>
                                
                                
                                <div class="contact-form-area">
                                    <form action="#" method="post">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="contact-name" placeholder="Name">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <input type="email" class="form-control" id="contact-email" placeholder="Email">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <input type="text" class="form-control" id="contact-number" placeholder="Website">
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <div class="form-group">
                                                    <textarea class="form-control" name="message" id="message" cols="30" rows="10" placeholder="Message"></textarea>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <button type="submit" class="btn crose-btn mt-15">Post Comment</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                            </div>
                        </div> -->
                    </div>
                </div>

                <!-- Blog Sidebar Area -->

                            <div class="col-12 col-sm-9 col-md-6 col-lg-3">
                    <div class="post-sidebar-area">

                        <!-- ##### Single Widget Area ##### -->
                        <div class="single-widget-area">
                            <div class="search-form">
                                <form action="#" method="get">
                                    <input type="search" name="search" placeholder="Rechercher">
                                    <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
                                </form>
                            </div>
                        </div>

                        <!-- ##### Single Widget Area ##### -->
                        <div class="single-widget-area">
                            <!-- Title -->
                            <div class="widget-title">
                                <h6>Articles</h6>
                            </div>

                            @foreach($latestPosts as $post)
                                <!-- Single Latest Posts -->
                                <div class="single-latest-post">
                                    <a href="{{route('sermon_show',[$post->id])}}" class="post-title">
                                        <h6>{{$post->title}}</h6>
                                    </a>
                                    <div class="sermons-meta-data">
                                        <p><i class="fa fa-user" aria-hidden="true"></i> Prédication de: <span>Pst. KASSEGNE Joseph</span></p>
                                        <!-- <p><i class="fa fa-tag" aria-hidden="true"></i> Categories: <span>God, Pray</span></p> -->
                                        <p><i class="fa fa-clock-o" aria-hidden="true"></i> March 10 on <span>9:00 am - 11:00 am</span></p>
                                    </div>
                                </div>

                            @endforeach

                            

                            

                        </div>

                        <!-- ##### Single Widget Area ##### -->
                        {{-- <div class="single-widget-area">
                            <!-- Title -->
                            <div class="widget-title">
                                <h6>Sermon Speaker</h6>
                            </div>
                            <ol class="crose-catagories">
                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Kyleigh Lam</a></li>
                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Thomas Jack</a></li>
                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> Garry Rick</a></li>
                                <li><a href="#"><i class="fa fa-angle-right" aria-hidden="true"></i> John Smith</a></li>
                            </ol>
                        </div> --}}

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Sermons Area End ##### -->
@endsection