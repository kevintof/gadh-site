@extends('frontend.layouts.app')
@section('AppCss')
<style>

.select2-container--bootstrap5 .select2-selection{
    box-shadow: none!important;
    height: auto;
    outline: 0!important;
}
.select2-container .select2-selection--single .select2-selection__rendered{
    padding-top: 10px;
}
.select2-container .select2-selection--single{
    height: 50px !important
}
.select2-container--default .select2-selection--single .select2-selection__arrow{
    height: 50px !important
}
</style>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection
 @section('content')
    <div class="contact-form section-padding-0-100">
        <div class="container">
            <div class="row">
                <!-- Section Heading -->
                <div class="col-12">
                    <div class="section-heading">
                        <h2>Cotisations</h2>
                        <p></p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <!-- Contact Form Area -->
                    <div class="contact-form-area">
                        <form >
                            <div class="row">
                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="contact-name">Nom*:</label>
                                        <input type="text" class="form-control" id="contact-name" placeholder="Nom complet">
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="contact-name">Prénoms*:</label>
                                        <input type="text" class="form-control" id="contact-name" placeholder="Nom complet">
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="contact-email">Email*:</label>
                                        <input type="email" class="form-control" id="contact-email" placeholder="contact@gadh.org">
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="contact-number">Numero:</label>
                                        <input type="text" class="form-control" id="contact-number" placeholder="(+228) 92493870">
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="payment" class="payment-type">Montant :</label>
                                        <input type="text" class="form-control" id="contact-number" placeholder=""/>
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="payment" class="payment-type">Groupes</label>
                                        <input type="text" class="form-control" id="contact-number" placeholder=""/>
                                    </div>
                                </div>

                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="payment" class="payment-type">Paiement :</label>
                                        <select id="payment" class=" form-control select2-icon" name="icon">
                                            <option value="en" class="test" data-thumbnail="https://dev.semoa-payments.ovh/bundles/habilitation/img/Cashpay.png">CASHPAY (TMoney,Flooz,etc.)</option>
                                            <option value="au" data-thumbnail="https://www.1min30.com/wp-content/uploads/2017/09/Paypal-logo-500x417.jpg">PayPal</option>
                                            <option value="uk" data-thumbnail="https://cdn-icons-png.flaticon.com/512/825/825482.png">Western Union</option>
                                            {{-- <option value="cn" data-thumbnail="https://upload.wikimedia.org/wikipedia/commons/thumb/3/39/NYCS-bull-trans-D.svg/2000px-NYCS-bull-trans-D.svg.png">German</option>
                                            <option value="de" data-thumbnail="https://upload.wikimedia.org/wikipedia/commons/thumb/0/04/MO-supp-E.svg/600px-MO-supp-E.svg.png">PayGates</option> --}}
                                        </select>
                                    </div>
                                    
                                </div>
                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="dons" class="payment-type">Type du don :</label>
                                        <select id="dons" class=" form-control select2-dons" name="icon">
                                            <option class="test" >DIMES</option>
                                            <option value="au"> Cotisation de groupes</option>
                                            <option value="uk"> Offrandes</option>
                                            <option value="uk"> Conférence internationale </option>
                                        </select>
                                    </div>
                                    
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="message">Commentaires*:</label>
                                        <textarea class="form-control" name="message" id="message" cols="30" rows="10" placeholder="Message"></textarea>
                                    </div>
                                </div>
                                
                            </div>
                        </form>
                        <div class="col-12 text-center">
                            <button  class="btn submit-btn crose-btn mt-15">Soumettre</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @section('js')
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script>

        function formatText (icon) {
            console.log($(icon.element))
            return $('<span><img height="20" width="20" src="' + $(icon.element).data('thumbnail') + '"/> ' + icon.text + '</span>');
        };

        $('.select2-icon').select2({
            width: "100%",
            
            templateSelection: formatText,
            templateResult: formatText
        });
        $('.select2-dons').select2({
            width: "100%",
            
        });
    </script>

    @endsection

 @endsection