@extends('frontend.layouts.app')
@section('headTitle')
     Contact
 @endsection
 @section("subbreadcrumb")
    Contact
@endsection
@section('AppCss')
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.7.1/dist/leaflet.css"
    integrity="sha512-xodZBNTC5n17Xt2atTPuE1HxjVMSvLVW9ocqUKLsCC5CXdbqCmblAshOMAS6/keqq/sMZMZ19scR4PsZChSR7A=="
    crossorigin=""/>
@endsection
@section('content')
    
     <!-- ##### Google Maps Start ##### -->
     <div  class="map-area mt-14">
        <div id="mapid">
            <!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22236.40558254599!2d-118.25292394686001!3d34.057682914027104!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2c75ddc27da13%3A0xe22fdf6f254608f4!2z4Kay4Ka4IOCmj-CmnuCnjeCmnOCnh-CmsuCnh-CmuCwg4KaV4KeN4Kav4Ka-4Kay4Ka_4Kar4KeL4Kaw4KeN4Kao4Ka_4Kav4Ka84Ka-LCDgpq7gpr7gprDgp43gppXgpr_gpqgg4Kav4KeB4KaV4KeN4Kak4Kaw4Ka-4Ka34KeN4Kaf4KeN4Kaw!5e0!3m2!1sbn!2sbd!4v1532328708137" allowfullscreen></iframe> --> 
        </div>
    </div>
    <!-- ##### Google Maps End ##### -->
    <!-- ##### Contact Area Start ##### -->
    <section class="contact-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="contact-content-area">
                        <div class="row">
                            <div class="col-12 col-md-4">
                                <div class="contact-content contact-information">
                                    <h4>Contact</h4>
                                    <p>missiongadhintl@gmail.org</p>
                                    <p>(+228) 92-49-38-70</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="contact-content contact-information">
                                    <h4>Adresse</h4>
                                    <p>Agbalepedogan, rue 294, à coté des rails ,près du cimetière</p>
                                    <p>Lome, Togo</p>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="contact-content contact-information">
                                    <h4>Heure d'ouverture</h4>
                                    <p>Mon-Sat: 10H à 18H</p>
                                    <p>Sunday: 07H à 15H</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Contact Area End ##### -->

    <!-- ##### Contact Form Area Start ##### -->
    <div class="contact-form section-padding-0-100">
        <div class="container">
            <div class="row">
                <!-- Section Heading -->
                <div class="col-12">
                    <div class="section-heading">
                        <h2>Laissez nous un message</h2>
                        <p></p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <!-- Contact Form Area -->
                    <div class="contact-form-area">
                        <form >
                            <div class="row">
                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="contact-name">Nom complet*:</label>
                                        <input type="text" class="form-control" id="contact-name" placeholder="Nom complet">
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="contact-email">Email*:</label>
                                        <input type="email" class="form-control" id="contact-email" placeholder="contact@gadh.org">
                                    </div>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <div class="form-group">
                                        <label for="contact-number">Numero:</label>
                                        <input type="text" class="form-control" id="contact-number" placeholder="(+228) 92493870">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="message">Message*:</label>
                                        <textarea class="form-control" name="message" id="message" cols="30" rows="10" placeholder="Message"></textarea>
                                    </div>
                                </div>
                                
                            </div>
                        </form>
                        <div class="col-12 text-center">
                            <button  class="btn submit-btn crose-btn mt-15">Soumettre</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ##### Contact Form Area End ##### -->
@endsection
@section('js')

   <script src="https://unpkg.com/leaflet@1.7.1/dist/leaflet.js"
   integrity="sha512-XQoYMqMTK8LvdxXYG3nZ448hOEQiglfqkJs1NOQV44cWnUrBc8PkAOcXy20w0vlaXaVUearIOBhiXZ5V3ynxwA=="
   crossorigin=""></script>
   <script>
         var mymap = L.map('mapid').setView([6.18965, 1.20562], 17);
         L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
         attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
         maxZoom: 18,
         }).addTo(mymap);

         L.marker([6.18965, 1.20562]).addTo(mymap)
         .bindPopup('Siege des eglises GADH')
         .openPopup();
    </script>
    
    <script>
        $(document).ready(function() {
            $('.submit-btn').click(function(){
              var data = {numero_telephone: $('#contact-number').val(),nom_complet:$('#contact-name').val(),message:$('#message').val(),email:$('#contact-email').val()};
               ajaxSend(data);

            })
            function ajaxSend(ajax_data) {

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            console.log('ccc');

            $.post({
                
                url: "/messages/post",
                dataType: "json",
                data: ajax_data,
                success: function(response, textStatus, jqXHR) {                  
                    console.log("coucou");                    
                },
                error: function(msg) {}
            });
            }

        });
    </script>
@endsection