<body>
    <!-- ##### Preloader ##### -->
    <div class="preloader d-flex align-items-center justify-content-center">
        <!-- Line -->
        <div class="line-preloader"></div>
    </div>

    <!-- ##### Header Area Start ##### -->
    <header class="header-area">

        <!-- ***** Top Header Area ***** -->
        <div class="top-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="top-header-content d-flex flex-wrap align-items-center justify-content-between">
                            <!-- Top Header Meta -->
                            <div class="top-header-meta d-flex flex-wrap">
                                <a href="#" class="open" data-toggle="tooltip" data-placement="bottom" title="10 Am to 6 PM"><i class="fa fa-clock-o" aria-hidden="true"></i> <span>Heure d'ouverture - 10 Am to 6 PM</span></a>
                                <!-- Social Info -->
                                <div class="top-social-info">
                                    <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                                    <a href="https://t.me/missiongadh"><i class="fa fa-telegram" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                                    <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                                </div>
                            </div>
                            <!-- Top Header Meta -->
                            <div class="top-header-meta">
                                <a href="mailto:missiongadhintl@gmail.com" class="email-address"><i class="fa fa-envelope" aria-hidden="true"></i> <span>missiongadhintl@gmail.com</span></a>
                                <a href="tel:+22899496088" class="phone"><i class="fa fa-phone" aria-hidden="true"></i> <span>+228 99496088</span></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ***** Top Header Area ***** -->

        <!-- ***** Navbar Area ***** -->
        <div class="crose-main-menu">
            <div class="classy-nav-container breakpoint-off">
                <div class="container">
                    <!-- Menu -->
                    <nav class="classy-navbar justify-content-between" id="croseNav">

                        <!-- Nav brand -->
                        <a href="{{route('main.index')}}" class="nav-brand"><img src="/img/GADH_edited.jpeg" class="logo" alt=""></a>

                        <!-- Navbar Toggler -->
                        <div class="classy-navbar-toggler">
                            <span class="navbarToggler"><span></span><span></span><span></span></span>
                        </div>

                        <!-- Menu -->
                        <div class="classy-menu">

                            <!-- close btn -->
                            <div class="classycloseIcon">
                                <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                            </div>

                            <!-- Nav Start -->
                            <div class="classynav">
                                <ul>
                                    <li><a href="{{route('main.index')}}">Accueil</a></li>
                                    {{-- <li><a href="#">Eglises GADH</a>
                                        <ul class="dropdown">
                                        <li><a href="{{route('main.churches')}}">Lome-agbalépédogan</a></li>
                                        <li><a href="{{route('main.churches')}}">Lomé-sagbado</a></li>
                                        <li><a href="{{route('main.churches')}}">Kara-tomdè</a></li>
                                            <li><a href="{{route('main.churches')}}">Californie(US)-Santa Rosa</a></li>
                                            <li><a href="{{route('main.churches')}}">Washington(US)-Beverly hills</a></li>
                                            <!-- <li><a href="blog.html">Blog</a></li>
                                            <li><a href="single-post.html">Blog Details</a></li>
                                            <li><a href="contact.html">Contact</a></li> -->
                                        </ul>
                                    </li> --}}
                                    <!-- <li><a href="#">Megamenu</a>
                                        <div class="megamenu">
                                            <ul class="single-mega cn-col-4">
                                                <li><a href="index.html">Home</a></li>
                                                <li><a href="about.html">About</a></li>
                                                <li><a href="sermons.html">Sermons</a></li>
                                                <li><a href="sermons-details.html">Sermons Details</a></li>
                                                <li><a href="events.html">Events</a></li>
                                                <li><a href="blog.html">Blog</a></li>
                                                <li><a href="single-post.html">Blog Details</a></li>
                                                <li><a href="contact.html">Contact</a></li>
                                            </ul>
                                            <ul class="single-mega cn-col-4">
                                                <li><a href="index.html">Home</a></li>
                                                <li><a href="about.html">About</a></li>
                                                <li><a href="sermons.html">Sermons</a></li>
                                                <li><a href="sermons-details.html">Sermons Details</a></li>
                                                <li><a href="events.html">Events</a></li>
                                                <li><a href="blog.html">Blog</a></li>
                                                <li><a href="single-post.html">Blog Details</a></li>
                                                <li><a href="contact.html">Contact</a></li>
                                            </ul>
                                            <ul class="single-mega cn-col-4">
                                                <li><a href="index.html">Home</a></li>
                                                <li><a href="about.html">About</a></li>
                                                <li><a href="sermons.html">Sermons</a></li>
                                                <li><a href="sermons-details.html">Sermons Details</a></li>
                                                <li><a href="events.html">Events</a></li>
                                                <li><a href="blog.html">Blog</a></li>
                                                <li><a href="single-post.html">Blog Details</a></li>
                                                <li><a href="contact.html">Contact</a></li>
                                            </ul>
                                            <ul class="single-mega cn-col-4">
                                                <li><a href="index.html">Home</a></li>
                                                <li><a href="about.html">About</a></li>
                                                <li><a href="sermons.html">Sermons</a></li>
                                                <li><a href="sermons-details.html">Sermons Details</a></li>
                                                <li><a href="events.html">Events</a></li>
                                                <li><a href="blog.html">Blog</a></li>
                                                <li><a href="single-post.html">Blog Details</a></li>
                                                <li><a href="contact.html">Contact</a></li>
                                            </ul>
                                        </div>
                                    </li> -->
                                    {{-- <li><a href="{{route('main.evenements')}}">Evènements</a></li>
                                    <li><a href="{{route('main.sermons')}}">Enseignements</a></li> --}}
                                    <!-- <li><a href="blog.html">Blog</a></li> -->
                                    <li><a href="#">Presentation</a>
                                        <ul class="dropdown">
                                            <li><a href="{{route('sermon_show',[3])}}">Mission de GADH</a></li>
                                            <li><a href="{{route('sermon_show',[4])}}">Vision de GADH</a></li>
                                            
                                            {{-- <li><a href="sermons.html">Sermons</a></li> --}}
                                            
                                        </ul>
                                    </li>
                                    
                                    <li><a href="{{route('main.contact')}}">Contact</a></li>
                                    <li><a href="{{route('direct')}}">GADH TV</a></li>
                                </ul>

                                <!-- Search Button -->
                                <div id="header-search"><i class="fa fa-search" aria-hidden="true"></i></div>

                                <!-- Donate Button -->
                                <a href="{{route('payment.index')}}" class="btn crose-btn header-btn">Faire un don </a>

                            </div>
                            <!-- Nav End -->
                        </div>
                    </nav>
                </div>
            </div>

            <!-- ***** Search Form Area ***** -->
            <div class="search-form-area">
                <div class="container">
                    <div class="row align-items-center">
                        <div class="col-12">
                            <div class="searchForm">
                                <form action="#" method="post">
                                    <input type="search" name="search" id="search" placeholder="Enter keywords &amp; hit enter...">
                                    <button type="submit" class="d-none"></button>
                                </form>
                                <div class="close-icon" id="searchCloseIcon"><i class="fa fa-close" aria-hidden="true"></i></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- ***** Navbar Area ***** -->
    </header>
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb pt-2 pb-2">
                            <li class="breadcrumb-item"><a href="#">Accueil</a></li>
                            {{-- <li class="breadcrumb-item active" aria-current="page">Evenements</li> --}}
                            <li class="breadcrumb-item active" aria-current="page">
                                @yield("subbreadcrumb")
                            </li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>