<section class="subscribe-area">
    <div class="container">
        <div class="row align-items-center">
            <!-- Subscribe Text -->
            <div class="col-12 col-lg-6">
                <div class="subscribe-text">
                    <h3>Abonnez vous à notre Newsletter</h3>
                    <h6>Abonnez vous et temoignez des bienfaits de Dieu</h6>
                </div>
            </div>
            <!-- Subscribe Form -->
            <div class="col-12 col-lg-6">
                <div class="subscribe-form text-right">
                    <form >
                        <input type="email" name="subscribe-email" id="subscribeEmail" placeholder="Votre e-mail">
                        <button class="btn crose-btn" type="button">S'abonner</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ##### Subscribe Area End ##### -->

<!-- ##### Footer Area Start ##### -->
<footer class="footer-area">
    <!-- Main Footer Area -->
    <div class="main-footer-area">
        <div class="container">
            <div class="row">

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-70">
                        <a href="#" class="footer-logo"><img class="logo" src="/img/GADH_edited.jpeg" alt=""></a>
                        <p>La paix du seigneur soit avec vous!</p>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-70">
                        <h5 class="widget-title">Liens utiles</h5>
                        <nav class="footer-menu">
                            <ul>
                                <li><a href="/"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Accueil</a></li>
                                <li><a href="{{route('main.contact')}}"><i class="fa fa-angle-double-right" aria-hidden="true"></i> Contact</a></li>
                                <li><a href="{{route('sermon_show',[4])}}"> <i class="fa fa-angle-double-right" aria-hidden="true"></i> Vision </a></li>
                                <li><a href="{{route('sermon_show',[3])}}"> <i class="fa fa-angle-double-right" aria-hidden="true"></i> Mission </a></li>
                                
                            </ul>
                        </nav>
                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-70">
                        <h5 class="widget-title">Dernières nouvelles</h5>

                        <!-- Single Latest News -->
                        <div class="single-latest-news">
                            <a href="#">Nouvelles réformes</a>
                            <p><i class="fa fa-calendar" aria-hidden="true"></i> November 11, 2017</p>
                        </div>

                    </div>
                </div>

                <!-- Single Footer Widget -->
                <div class="col-12 col-sm-6 col-lg-3">
                    <div class="single-footer-widget mb-70">
                        <h5 class="widget-title">Contacts</h5>

                        <div class="contact-information">
                            <p><i class="fa fa-map-marker" aria-hidden="true"></i> Agbalépédogan, Lomé, Togo</p>
                            <a href="tel:+22899496088"><i class="fa fa-phone" aria-hidden="true"></i> +228 99496088</a>
                            <a href="mailto:missiongadhintl@gmail.com"><i class="fa fa-envelope" aria-hidden="true"></i> missiongadhintl@gmail.com</a>
                            <p><i class="fa fa-clock-o" aria-hidden="true"></i> Lun - Ven: 08.00am - 18.00pm</p>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Copwrite Area -->
    <div class="copywrite-area">
        <div class="container h-100">
            <div class="row h-100 align-items-center flex-wrap">
                <!-- Copywrite Text -->
                <div class="col-12 col-md-6">
                    <div class="copywrite-text">
                        <p><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | Dévéloppé par <i class="fa fa-heart-o" aria-hidden="true"></i>  <a href="https://colorlib.com" target="_blank">GADH DEV TEAM</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</p>
                    </div>
                </div>

                <!-- Footer Social Icon -->
                <div class="col-12 col-md-6">
                    <div class="footer-social-icon">
                        <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="https://t.me/missiongadh"><i class="fa fa-telegram" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-google-plus" aria-hidden="true"></i></a>
                        <a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- ##### Footer Area End ##### -->

<!-- ##### All Javascript Script ##### -->
<!-- jQuery-2.2.4 js -->
<script src="{{asset('js/jquery/jquery-2.2.4.min.js')}}"></script>
<!-- Popper js -->
<script src="{{asset('js/bootstrap/popper.min.js')}}"></script>
<!-- Bootstrap js -->
<script src="{{asset('js/bootstrap/bootstrap.min.js')}}"></script>
<!-- All Plugins js -->
<script src="{{asset('js/plugins/plugins.js')}}"></script>
<!-- Active js -->
<script src="{{asset('js/active.js')}}"></script>
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
{{-- <script src="{{asset('js/plugins/.js')}}"></script> --}}
<script>
   AOS.init();
   
</script>
@yield('js')
</body>

</html>