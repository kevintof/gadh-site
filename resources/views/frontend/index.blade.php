@extends('frontend.layouts.app')
 @section('headTitle')
     Accueil
 @endsection
 @section('headTitle')
     Accueil
 @endsection
@section('content')
     <!-- ##### Hero Area Start ##### -->
    <section id="car" class="hero-area hero-post-slides owl-carousel">
        
        <div data-dot="<button role='button' ></button>" class="item single-hero-slide bg-img bg-overlay d-flex align-items-center justify-content-center" style="background-image: url(img/conference.jpg);">
            <!-- Post Content -->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="hero-slides-content">
                            <h2 data-animation="fadeInUp" data-delay="100ms">Conférence internationale GADH</h2>
                            <p data-animation="fadeInUp" data-delay="300ms">Habacuc 2:2-3</p>
                            <a href="https://form.mission-gadh.com/index.php/formulaire-dinscription/" class="btn crose-btn" data-animation="fadeInUp" data-delay="500ms">Inscrivez vous</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Single Hero Slide -->
        <div data-dot="<button role='button' ></button>" class="item single-hero-slide bg-img bg-overlay d-flex align-items-center justify-content-center" style="background-image: url(img/bg-img/back_gadh_acc.jpg);">
            <!-- Post Content -->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="hero-slides-content">
                            <h2 data-animation="fadeInUp" data-delay="100ms">Batir la foi</h2>
                            <p data-animation="fadeInUp" data-delay="300ms">Habacuc 2:2-3</p>
                            <a href="{{route('main.contact')}}" class="btn crose-btn" data-animation="fadeInUp" data-delay="500ms">A propos</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Single Hero Slide -->
        <div data-dot="<button role='button' ></button>" class="item single-hero-slide bg-img bg-overlay d-flex align-items-center justify-content-center" style="background-image: url(img/bg-img/gen_choisie.jpeg);">
            <!-- Post Content -->
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="hero-slides-content">
                            <h2 data-animation="fadeInUp" data-delay="100ms">Gagner les âmes de la dernière heure (GADH)</h2>
                            <p data-animation="fadeInUp" data-delay="300ms">Mathieu 4:16</p>
                            <a href="#" class="btn crose-btn" data-animation="fadeInUp" data-delay="500ms">Contactez nous</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        


    </section>
    <!-- ##### Hero Area End ##### -->
     <!-- ##### About Area Start ##### -->
    
     <div data-aos="fade-right"
    
    data-aos-duration="10000">
        <section class="about-area section-padding-100-0">
            <div class="container">
                <div class="row">
                    <!-- Section Heading -->
                    <div class="col-12">
                        <div class="section-heading">
                            <h2>Bienvenue à l'église</h2>
                            <p>Nous nous rencontrons généralement au Togo, aux Etats-unis. Avec l'église en ligne rejoignez ou que vous soyez.</p>
                        </div>
                    </div>
                </div>

                <div class="row about-content justify-content-center">
                    <!-- Single About Us Content -->
                    @foreach ($postDescs as $postDesc)
                        <div class="col-12 col-md-6 col-lg-4">
                            <div class="about-us-content mb-100">
                                <img src="img/GADH_edited.jpeg" alt="">
                                <div class="about-text">
                                    <h4>{{ $postDesc->title }}</h4>
                                    <p class="short-paragraph">{{$postDesc->body}}</p>
                                    <a href="{{route('sermon_show',[$postDesc->id])}}">Lire plus <i class="fa fa-angle-double-right"></i></a>
                                </div>
                            </div>
                        </div>
                    @endforeach

                    <!-- Single About Us Content -->
                    <!--<div class="col-12 col-md-6 col-lg-4">
                        <div class="about-us-content mb-100">
                            {{-- <img src="img/bg-img/4.jpg" alt=""> --}}
                            <img src="img/gadh.jpg" alt="">
                            <div class="about-text">
                                <h4>Notre histoire</h4>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                <a href="#">Lire plus <i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>-->

                    <!-- Single About Us Content -->
                    <!--<div class="col-12 col-md-6 col-lg-4">
                        <div class="about-us-content mb-100">
                            {{-- <img src="img/bg-img/5.jpg" alt=""> --}}
                            <img src="img/gadh.jpg" alt="">
                            <div class="about-text">
                                <h4>Nos enseignements</h4>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form.</p>
                                <a href="#">Lire plus <i class="fa fa-angle-double-right"></i></a>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
        </section>
   </div>
    <!-- ##### About Area End ##### -->

    <!-- ##### Call To Action Area Start ##### -->
    <section class="call-to-action-area section-padding-100 bg-img bg-overlay" style="background-image: url(img/bg-img/back_gadh_ad.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="call-to-action-content text-center">
                        <h6>Une place pour vous</h6>
                        <h2>Trouvez une assemblée dans laquelle adorer</h2>
                        <a href="#" class="btn crose-btn btn-2">Devenez membre</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Call To Action Area End ##### -->

    <!-- ##### Latest Sermons Area Start ##### -->
    <section class="latest-sermons-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <!-- Section Heading -->
                <div class="col-12">
                    <div class="section-heading">
                        <h2>Derniers enseignements</h2>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <!-- Single Latest Sermons -->
                {{-- <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-latest-sermons mb-100">
                        <div class="sermons-thumbnail">
                            <img src="img/bg-img/7.jpg" alt="">
                            <!-- Date -->
                            <div class="sermons-date">
                                <h6><span>10</span>MAR</h6>
                            </div>
                        </div>
                        <div class="sermons-content">
                            <div class="sermons-cata">
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Video"><i class="fa fa-video-camera" aria-hidden="true"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Audio"><i class="fa fa-headphones" aria-hidden="true"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Document"><i class="fa fa-file" aria-hidden="true"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Télécharger"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                            </div>
                            <h4>Changez vos habitudes de vie</h4>
                            <div class="sermons-meta-data">
                                <p><i class="fa fa-user" aria-hidden="true"></i> Orateur: <span>Jorge Malone</span></p>
                                <p><i class="fa fa-tag" aria-hidden="true"></i> Categories: <span>God, Pray</span></p>
                                <p><i class="fa fa-clock-o" aria-hidden="true"></i> March 10 on <span>9:00 am - 11:00 am</span></p>
                            </div>
                        </div>
                    </div>
                </div> --}}
                
                <!-- Single Latest Sermons -->
                @foreach( $enseignements as $ens )
                    <div class="col-12 col-md-6 col-lg-4">
                        <div class="single-latest-sermons mb-100">
                            <div class="sermons-thumbnail">
                                <img src="img/bg-img/8.jpg" alt="">
                                <!-- Date -->
                                <div class="sermons-date">
                                    <h6><span>{{ date('d', strtotime($ens->date)) }}</span>{{$months[date('m', strtotime($ens->date))-1]}} {{ date('Y', strtotime($ens->date)) }}</h6>
                                </div>
                            </div>
                            <div class="sermons-content">
                                <div class="sermons-cata">
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="Video"><i class="fa fa-video-camera" aria-hidden="true"></i></a>
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="Audio"><i class="fa fa-headphones" aria-hidden="true"></i></a>
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="Docs"><i class="fa fa-file" aria-hidden="true"></i></a>
                                    <a href="#" data-toggle="tooltip" data-placement="top" title="Télécharger"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                                </div>
                                <a href="{{route('sermon_show',[$ens->id])}}"><h5 class="short-paragraph-medium">{{$ens->title}}</h5></a>
                                <div class="sermons-meta-data">
                                    <p><i class="fa fa-user" aria-hidden="true"></i> Orateur: <span>Pasteur KASSEGNE Joseph</span></p>
                                    <p><i class="fa fa-tag" aria-hidden="true"></i> Categories: <span>God, Pray</span></p>
                                    <p><i class="fa fa-clock-o" aria-hidden="true"></i>  11 Mars on <span>10:00 am - 11:00 am</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                <!-- Single Latest Sermons -->
                {{-- <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-latest-sermons mb-100">
                        <div class="sermons-thumbnail">
                            <img src="img/bg-img/9.jpg" alt="">
                            <!-- Date -->
                            <div class="sermons-date">
                                <h6><span>15</span>Mai</h6>
                            </div>
                        </div>
                        <div class="sermons-content">
                            <div class="sermons-cata">
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Video"><i class="fa fa-video-camera" aria-hidden="true"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Audio"><i class="fa fa-headphones" aria-hidden="true"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Docs"><i class="fa fa-file" aria-hidden="true"></i></a>
                                <a href="#" data-toggle="tooltip" data-placement="top" title="Télécharger"><i class="fa fa-cloud-download" aria-hidden="true"></i></a>
                            </div>
                            <h4>Le retour du christ</h4>
                            <div class="sermons-meta-data">
                                <p><i class="fa fa-user" aria-hidden="true"></i> Orateur: <span>Pst KASSEGENE Joseph</span></p>
                                <p><i class="fa fa-tag" aria-hidden="true"></i> Categories: <span>God, Pray</span></p>
                                <p><i class="fa fa-clock-o" aria-hidden="true"></i> March 10 on <span>9:00 am - 11:00 am</span></p>
                            </div>
                        </div>
                    </div>
                </div> --}}
            </div>
        </div>
    </section>
    <!-- ##### Latest Sermons Area End ##### -->

    <!-- ##### Upcoming Events Area Start ##### -->
    <section class="upcoming-events-area section-padding-0-100">
        <!-- Upcoming Events Heading Area -->
        <div class="upcoming-events-heading bg-img bg-overlay bg-fixed" style="background-image: url(img/bg-img/1.jpg);">
            <div class="container">
                <div class="row">
                    <!-- Section Heading -->
                    <div class="col-12">
                        <div class="section-heading text-left white">
                            <h2>Prochains évènements</h2>
                            <p>Abonnez vous à notre newsletters pour etre au parfum des évènements</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Upcoming Events Slide -->
        <div class="upcoming-events-slides-area">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="upcoming-slides owl-carousel">
                            @foreach( $events as $event )
                                <div class="single-slide">
                                    <!-- Single Upcoming Events Area -->
                                    <div class="single-upcoming-events-area d-flex flex-wrap align-items-center">
                                        <!-- Thumbnail -->
                                        <div class="upcoming-events-thumbnail">
                                            <img src="{{asset('img/'.$event->cover_image)}}" alt="">
                                        </div>
                                        <!-- Content -->
                                        <div class="upcoming-events-content d-flex flex-wrap align-items-center">
                                            <div class="events-text">
                                                <h4>{{ $event->title }}</h4>
                                                <div class="events-meta">
                                                    <a href="#"><i class="fa fa-calendar" aria-hidden="true"></i> {{$event->date}}</a>
                                                    <a href="#"><i class="fa fa-clock-o" aria-hidden="true"></i> 09:00 - 11:00</a>
                                                    <a href="#"><i class="fa fa-map-marker" aria-hidden="true"></i> {{ $event->location }}</a>
                                                </div>
                                                <p class="short-paragraph"> {{$event->body}}</p>
                                                <a href="{{route('event_show',[$event->id])}}">Lire plus <i class="fa fa-angle-double-right"></i></a>
                                            </div>
                                            <div class="find-out-more-btn">
                                                <a href="{{route('event_show',[$event->id])}}" class="btn crose-btn btn-2">Voir plus</a>
                                            </div>
                                        </div>
                                    </div>

                                    
                                </div>
                            @endforeach

                            

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- ##### Upcoming Events Area End ##### -->

    <!-- ##### Gallery Area Start ##### -->
    <div class="gallery-area d-flex flex-wrap">
        <!-- Single Gallery Area -->
        <div class="single-gallery-area">
            <a href="img/bg-img/sport.jpeg" class="gallery-img" title="Gallery Image 1">
                <img src="img/bg-img/sport.jpeg" alt="">
            </a>
        </div>

        <!-- Single Gallery Area -->
        <div class="single-gallery-area">
            <a href="img/bg-img/parents.jpg" class="gallery-img" title="Gallery Image 2">
                <img src="img/bg-img/parents_rog.jpg" alt="">
            </a>
        </div>

        <!-- Single Gallery Area -->
        <div class="single-gallery-area">
            <a href="img/bg-img/leaders.jpg" class="gallery-img" title="Gallery Image 3">
                <img src="img/bg-img/leaders_rog.jpg" alt="">
            </a>
        </div>

        <!-- Single Gallery Area -->
        <div class="single-gallery-area">
            <a href="img/bg-img/young_women.jpg" class="gallery-img" title="Gallery Image 4">
                <img src="img/bg-img/young_women_rog.jpg" alt="">
            </a>
        </div>

        <!-- Single Gallery Area -->
        <div class="single-gallery-area">
            <a href="img/bg-img/men.jpg" class="gallery-img" title="Gallery Image 5">
                <img src="img/bg-img/men.jpg" alt="">
            </a>
        </div>

        <!-- Single Gallery Area -->
        <div class="single-gallery-area">
            <a href="img/bg-img/soldiers.jpg" class="gallery-img" title="Gallery Image 6">
                <img src="img/bg-img/soldiers_rog.jpg" alt="">
            </a>
        </div>

        <!-- Single Gallery Area -->
        <div class="single-gallery-area">
            <a href="img/bg-img/mommy.jpg" class="gallery-img" title="Gallery Image 7">
                <img src="img/bg-img/mommy_rog.jpg" alt="">
            </a>
        </div>

        <!-- Single Gallery Area -->
        <div class="single-gallery-area">
            <a href="img/bg-img/daddy_mommy.jpg" class="gallery-img" title="Gallery Image 8">
                <img src="img/bg-img/daddy_mommy_rog.jpg" alt="">
            </a>
        </div>

        <!-- Single Gallery Area -->
        <div class="single-gallery-area">
            <a href="img/bg-img/pic_nic.jpg" class="gallery-img" title="Gallery Image 9">
                <img src="img/bg-img/pic_nic_rog.jpg" alt="">
            </a>
        </div>

        <!-- Single Gallery Area -->
        <div class="single-gallery-area">
            <a href="img/bg-img/22.jpg" class="gallery-img" title="Gallery Image 10">
                <img src="img/bg-img/22.jpg" alt="">
            </a>
        </div>
    </div>
    <!-- ##### Gallery Area End ##### -->

    <!-- ##### Blog Area Start ##### -->
    {{-- <section class="blog-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <!-- Section Heading -->
                <div class="col-12">
                    <div class="section-heading">
                        <h2>dernières nouvelles</h2>
                        <p>les dernières informations concernant l'église, les réligions et le monde</p>
                    </div>
                </div>
            </div>

            <div class="row justify-content-center">
                <!-- Single Blog Post Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-blog-post mb-100">
                        <div class="post-thumbnail">
                            <a href="single-post.html"><img src="img/bg-img/10.jpg" alt=""></a>
                        </div>
                        <div class="post-content">
                            <a href="single-post.html" class="post-title">
                                <h4>La main de Dieu</h4>
                            </a>
                            <div class="post-meta d-flex">
                                <a href="#"><i class="fa fa-user" aria-hidden="true"></i> Luke Coppen</a>
                                <a href="#"><i class="fa fa-calendar" aria-hidden="true"></i> April 23, 2018</a>
                            </div>
                            <p class="post-excerpt">The priest, who was also the diocesan judicial vicar, was accosted by the assailant and was involved in a discussion.</p>
                        </div>
                    </div>
                </div>

                <!-- Single Blog Post Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-blog-post mb-100">
                        <div class="post-thumbnail">
                            <a href="single-post.html"><img src="img/bg-img/11.jpg" alt=""></a>
                        </div>
                        <div class="post-content">
                            <a href="single-post.html" class="post-title">
                                <h4>Nouvelles réformes</h4>
                            </a>
                            <div class="post-meta d-flex">
                                <a href="#"><i class="fa fa-user" aria-hidden="true"></i> Staff Reporter</a>
                                <a href="#"><i class="fa fa-calendar" aria-hidden="true"></i> April 03, 2018</a>
                            </div>
                            <p class="post-excerpt">The Liturgy helps us to “rediscover our identity as disciples of the Risen Lord”, Pope Francis said at the Regina Caeli.</p>
                        </div>
                    </div>
                </div>

                <!-- Single Blog Post Area -->
                <div class="col-12 col-md-6 col-lg-4">
                    <div class="single-blog-post mb-100">
                        <div class="post-thumbnail">
                            <a href="single-post.html"><img src="img/bg-img/12.jpg" alt=""></a>
                        </div>
                        <div class="post-content">
                            <a href="single-post.html" class="post-title">
                                <h4>Nomination d'un nouveau diacre</h4>
                            </a>
                            <div class="post-meta d-flex">
                                <a href="#"><i class="fa fa-user" aria-hidden="true"></i> Lucie Smith</a>
                                <a href="#"><i class="fa fa-calendar" aria-hidden="true"></i> April 15, 2018</a>
                            </div>
                            <p class="post-excerpt">God comes to us in free and undeserved favor in the person of Jesus Christ who lived, died, and rose for us that we might belong to God.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> --}}
    <!-- ##### Blog Area End ##### -->
                                    
@endsection