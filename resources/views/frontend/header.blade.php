<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Title -->
    <title>@yield('headTitle')</title>
    <meta property="og:description" content="Mission chrétienne Gagner les Ames de la Dernière Heure (GADH)" />
    <meta property="og:image" content="{{asset('img/gadh.jpg')}}" />
    <link
        rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
    />
    <!-- Favicon -->
    <link rel="icon" href="{{asset('img/gadhc.ico')}}">
    <!-- Core Stylesheet -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}">
    <link rel="stylesheet" href="{{asset('css/custom.css')}}"> 
    <link rel="stylesheet" href="{{asset('css/welcomecarousel.css')}}"> 
    
    @yield('AppCss')
</head>