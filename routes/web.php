<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */
Route::get('/', 'MainController@index')->name('main.index');
Route::get('/contact', 'MainController@contact')->name('main.contact');
Route::get('/evenements','MainController@events')->name('main.evenements');
Route::get('/enseignements','MainController@sermons')->name('main.sermons');
Route::get('/enseignements','MainController@sermons');
Route::get('/enseignements/{id}','MainController@sermon_show')->name('sermon_show');
Route::get('/events/{id}','MainController@event_show')->name('event_show');
Route::get('/enseignement_detail','MainController@sermon_details')->name('main.sermon_detail');
Route::get('/eglises','MainController@churches')->name('main.churches');
Route::get('/users/subscribe','FrontControllers/UserController@newsletters_subscribe')->name('subscribe.newsletters');

Route::post('/messages/post','FrontControllers\MessageController@create')->name('send_message');
Route::get('/gadh-tv','DirectController@direct')->name('direct');
Route::get('/gadh-donation','PaymentController@pagePnayment')->name('payment.index');

Route::get('/backend/index','BackControllers\MainController@index')->name('backend.index');
Route::get('/backend/login','BackControllers\AuthController@login')->name('backend.login');

Route::get('/website/live',function(){
    return Artisan::call('up');
});

Route::get('/website/down',function(){
    return Artisan::call('down');
});