<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gadh_messages', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('nom_complet')->nullable();
            $table->string('email')->nullable();
            $table->string('numero_telephone')->nullable();
            $table->string('message')->nullable();
             
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages_tables');
    }
}
