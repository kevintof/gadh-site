<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableGadhFichiers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gadh_fichiers', function (Blueprint $table) {
            $table->id();
            $table->string("nom")->nullable();
            $table->string("telegram_file_link")->nullable();
            $table->boolean("is_image")->default(false);
            $table->boolean("is_document")->default(false);
            $table->boolean("is_audio")->default(false);
            $table->bigInteger("post_id")->unsigned()->nullable();
            $table->foreign('post_id')
                ->references('id')
                ->on('gadh_posts')
                ->onDelete('restrict')
                ->onUpdate('restrict');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gadh_fichiers');
    }
}
