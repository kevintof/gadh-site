<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class GadhPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gadh_posts', function (Blueprint $table) {
            $table->id();
            $table->string("title")->nullable();
            $table->longText("body")->nullable();
            $table->string("orateur")->nullable();
            $table->date("date")->nullable();
            $table->time("heure_debut")->nullable();
            $table->time("heure_fin")->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gadh_posts');
    }
}
