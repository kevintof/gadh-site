<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateTablePost extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('gadh_posts', function (Blueprint $table) {
            //
            $table->bigInteger("type_id")->unsigned()->nullable();
            $table->foreign('type_id')
                ->references('id')
                ->on('gadh_posts_types')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('gadh_posts', function (Blueprint $table) {
            //
        });
    }
}
