<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostEnseignementSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        $posts = [
            [
                "title" => "L'OBÉISSANCE À DIEU SOURCE DE BÉNÉDICTION",
                "body" =>"Jésus apprenant à prier à ces disciples leur dit dans Mathieu le chapitre 6 verset 10; que ton règne vienne, que ta volonté soit faite sur la terre comme au ciel. Le plan de Dieu pour nous, c'est qu'il soit exercé son pouvoir. Jésus-Christ enseignant cette prière à ces disciples voulait qu'ils comprennent, qu'ils voient l'importance de l'exercice du pouvoir de Dieu sur notre vie. Nous devons être à l'écoute de Dieu, nous soumettre à Dieu,  dépendre de lui; telle est sa volonté, et c'est exactement ce qui se passe dans le ciel et cela doit être ainsi sur la terre. Jésus veut que l'on se soumette entièrement, et totalement à Dieu; que son pouvoir souverain s'exerce entièrement et totalement sur nous, en d'autre terme, que nous soyons entièrement soumis à son autorité, dépendant de lui, vivant pour lui et par lui; telle est la volonté de Dieu pour nous. Alléluia, la bénédiction vient quand son règne vient dans notre vie, se manifeste dans notre vie, et au travers de nos vies. La bénédiction vient quand on est soumis, la bénédiction vient dans notre vie quand on apprend à le procurer ou bien à le donner, à lui faire plaisir plutôt; toutefois, malgré que l'on se trouve à bord de la même barque que Christ, c'est à dire, en relation parfaite avec Christ, le vent peut souffler, même en étant en bonne relation avec Dieu, le vent ne manquera pas de souffler; seulement avec lui, ce vent se taira, il va se taire; voilà la différence. Alléluia, ce qui est intéressant ici, c'est que le vent peut souffler bien qu'on soit en parfaite relation avec christ, une relation avec christ dans une communion harmonieuse. Le vent peut souffler, le vent de la sécheresse aussi peut souffler; mais ce qui est intéressant, ce qui fait la différence, c'est que avec christ, en d'autre terme au nom de Jésus-Christ on peut se lever et ordonner à un vent de se taire et il obéira.",
                "cover_image" => null,
                "type_id" => 2
            ],
            [
                "title" => "LE POUVOIR D'UN VŒU",
                "body" =>"Bien aimée dans le seigneur, nous allons nous intéresser au livre de Juges 11 verset 29 à 40. La bible déclare que le peuple de Dieu devait faire face au peuple d'Ammon. Aussi, Jephthé pria et confia l'affaire a l'Éternel. Alléluia! Au delà de ce que demande Jephthé, il fit un vœu a l'Éternel. Il promis à l'Éternel de sacrifier toute personne qui sortira de chez lui à son heureux retour. Alléluia! Dans la suite du passage, nous verrons qu'effectivement, Dieu sera avec lui, et Jephthé va emporter une éclatante victoire. Dans ce contexte triomphale, il entra chez lui; et lorsque sa propre fille appris la nouvelle, et sachant que son père revenait; il était à quelque mètre de la maison, et sa fille sortit brusquement pour lui saluer. Elle fut la première personne à rencontrer son père; alors que le père avait fait un vœu d'offrir en sacrifice quiconque sortira le premier à son retour. Pourquoi un tel vœu? Alléluia! Vous comprenez bien le pouvoir d'un vœu. Jephthé avait connaissance de ce que c'est que vœu, et il savait qu'à partir d'un vœu se crée des liens, il se crée une alliance; autrement dit le lien à Dieu par rapport au problème qu'il a soumis à Dieu. Donc ce vœu la devient en même temps le soubassement d'une alliance entre lui et Dieu, où les deux parties sont tenues au respect relatif de cet alliance.
                Bien aimée dans le seigneur, dans la suite de la lecture, vous verrez que Jephthé sera dans l'obligation d'offrir sa fille en sacrifice malgré les peines que cela pouvaient engendrer. Il pouvait l'imaginer, si cela était possible de s'en passer; mais bien aimée dans le seigneur, ce qui retient notre attention, ici, c'est que Jephthé a pris le courage malgré sa souffrance de sacrifier son propre enfant. La bible déclare qu'il l'aimait beaucoup. C'est tellement difficile qu'on hôte la vie de quelqu'un sans que ce dernier ait fait quelque chose.
             Bien aimée dans le seigneur, nous tenons ici à souligner que le vœu revêt une importance capitale. L'on ne peut s'amuser avec un vœu, on ne peut traiter un vœu avec légèreté; c'est impossible. Alléluia! C'est la raison pour laquelle quand bien même il était dépassé par les évènements, Jephthé s'est vue dans l'obligation d'honorer à son engagement.
             Bien aimée dans le seigneur, nous venons par ce message, vous exhorter a faire beaucoup attention de ne pas traiter avec légèreté un vœu, sinon cela ne vaut pas la peine d'en faire; car le vœu en lui même représente une force, elle nous introduit dans une alliance; on ne peut le traité avec légèreté; cela comporte des bénéfices bien sûr, mais cela comporte aussi des grands risques; car le vœu produit toujours d'effet, peut importe la situation dans laquelle on a fait le vœu; mais le vœu reste un vœu, alléluia, et nous devons donc faire beaucoup attention, parce que nous ne devons pas faire un vœu que nous ne sommes pas en mesure d'honorer; cela va à votre désavantage, parce que nous avons fait le vœux aux esprits du monde des ténèbres, et cela devient une menace.
             Quand vous étiez sans Christ, et le pouvoir que représente ces vœux sont incroyable.
             Nous devons avoir les moyens nécessaires avant de le faire. Que le seigneur vous bénisse bien aimée. Amen.
             ",
                "cover_image" => null,
                "type_id" => 2
            ],
            
        ];

        for($i = 0 ; $i < sizeof($posts) ; $i++){
            DB::table('gadh_posts')->insert([
                // 'id' => ($i+1),
                'title' => $posts[$i]["title"],
                'body' => $posts[$i]["body"],
                'type_id' => $posts[$i]["type_id"],
                "cover_image" => $posts[$i]["cover_image"]
            ]);
        }
    }
}
