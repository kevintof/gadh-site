<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostDescriptionSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $posts = [
            [
                "title" => "Lorem Ipsum",
                "body" =>"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum auctor arcu ut dui volutpat, ut imperdiet sapien egestas. Nunc ante dui, dapibus nec augue non, maximus maximus est. Nulla varius dolor magna, non vulputate odio fringilla quis. Nunc id ipsum ac orci cursus mollis at non ipsum. Maecenas id maximus ante. Integer maximus, justo et venenatis aliquam, enim nisi ultricies ipsum, quis sodales risus ipsum in quam. Nullam et lectus turpis. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Integer lectus urna, auctor a mattis eu, pulvinar vel lectus. Quisque arcu nibh, maximus eu orci et, dictum consequat est. Aenean volutpat ipsum purus, a rutrum turpis tristique at. Fusce hendrerit libero sit amet scelerisque dignissim. Ut imperdiet posuere nibh, quis commodo ex mattis et.",
                "cover_image" => null,
                "type_id" => 1
            ],
            [
                "title" => "Proin sagittis",
                "body" =>"Proin sagittis tempor dui nec mattis. Etiam pulvinar, metus ac mollis placerat, sem dui aliquet metus, et dapibus lectus odio eu massa. Etiam in fermentum dui. Nam sit amet tempus libero, non egestas lorem. Proin ac congue est, at sodales ex. Cras tellus urna, consectetur at leo rutrum, pretium luctus dui. Praesent faucibus, sapien et ullamcorper interdum, sem libero aliquet eros, at sodales tortor mauris sed ante. In porttitor mauris eget porta commodo. Aenean mollis tortor quis commodo porttitor. Ut sed vehicula velit. Pellentesque nunc enim, venenatis in orci in, vulputate feugiat metus. Mauris vitae pellentesque purus. Fusce varius risus ac sodales euismod.",
                "cover_image" => null,
                "type_id" => 1
            ]
        ];

        for($i = 0 ; $i < sizeof($posts) ; $i++){
            DB::table('gadh_posts')->insert([
                //'id' => ($i+1),
                'title' => $posts[$i]["title"],
                'body' => $posts[$i]["body"],
                'type_id' => $posts[$i]["type_id"],
                "cover_image" => $posts[$i]["cover_image"]
            ]);
        }
    }

}
?>