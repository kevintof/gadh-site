<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EventsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $events = [
            [
                "title" => "15e anniversaire",
                "location" => "Agbalepedogan, rue 294, à coté des rails ,près du cimetière Lome, Togo",
                "body" => "Lorem ipsum dolor sit amet. Qui numquam incidunt qui commodi officia sit vitae ducimus qui illum molestias sit laudantium placeat. Qui dolore voluptatem sed incidunt illum et sapiente officiis est ipsa eligendi est ducimus autem et quasi minima At magni aperiam! Aut eius facilis ut voluptates voluptatum et sunt molestias qui corrupti voluptatibus eum quia illum eos corrupti quos aut eius praesentium.",
                "date" =>"2023-03-15",
                "cover_image" => "anniversaire.png",
                "type_id" => 3
            ],
            [
                "title" => "Conférence internationale de la mission GADH",
                "location" => "Hotel EDA OBA, Tokoin ,Lomé",
                "body" => "Lorem ipsum dolor sit amet. Qui numquam incidunt qui commodi officia sit vitae ducimus qui illum molestias sit laudantium placeat. Qui dolore voluptatem sed incidunt illum et sapiente officiis est ipsa eligendi est ducimus autem et quasi minima At magni aperiam! Aut eius facilis ut voluptates voluptatum et sunt molestias qui corrupti voluptatibus eum quia illum eos corrupti quos aut eius praesentium.",
                "date" => "2023-07-20",
                "cover_image" => "conference.jpg",
                "type_id" => 3
            ]

            

        ];

        for($i = 0 ; $i < sizeof($events) ; $i++){
            DB::table('gadh_posts')->insert([
                //'id' => ($i+1),
                'title' => $events[$i]["title"],
                "date" => $events[$i]["date"],
                "location" => $events[$i]["location"],
                'body' => $events[$i]["body"],
                'type_id' => $events[$i]["type_id"],
                "cover_image" => $events[$i]["cover_image"]
            ]);
        }
    }
}
