<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PostTypeSeeder::class,
            PostEnseignementSeeder::class,
            PostDescriptionSeeder::class,
            EventsSeeder::class
        ]);
        // $this->call(UserSeeder::class);
    }
}
