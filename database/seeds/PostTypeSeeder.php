<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $postTypes = [
            'description', 'enseignement', 'evenement', 'annonce'
        ];
        //
        for ($i = 0; $i < sizeof($postTypes); $i++) {
            DB::table('gadh_posts_types')->insert([
                'id' => ($i+1),
                'nom' => $postTypes[$i]
            ]); 
        } 
    }
}
